<?php

// Récupération des données
if(isset($_GET['id'])){
    $id = (int)$_GET['id'];
}

if(isset($_GET['published'])){
    $published = (int)!(bool)$_GET['published'];
}


// Mise à jour des données
require('params.php');
$DBH = new PDO(sprintf('mysql:host=%s;dbname=%s', $host, $dbname), $user, $pass);
$STH = $DBH->prepare("UPDATE tutos SET published=:published WHERE id=:id");
$STH->bindParam(':published', $published);
$STH->bindParam(':id', $id);
$STH->execute();

header('location:index.php');