DROP TABLE IF EXISTS `tutos`;
CREATE TABLE `tutos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tutos` (`id`, `title`, `published`) VALUES
(1,	'Composer',	1),
(2,	'PDO',	1),
(3,	'Ajax',	1),
(4,	'JSON',	NULL),
(6,	'Symfony',	NULL);