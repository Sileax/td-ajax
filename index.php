<!doctype html>

<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Tutos</title>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link rel="stylesheet" href="app.css">
    </head>
<body>

<?php
require('params.php');
$DBH = new PDO(sprintf('mysql:host=%s;dbname=%s', $host, $dbname), $user, $pass);

# Récupération depuis la base de données
$STH = $DBH->prepare('SELECT * from tutos');
$STH->execute();

?>

<section class="container">

    <h1>Tutos</h1>

    <ul id="tutos">

        <?php

        while($row = $STH->fetch(PDO::FETCH_ASSOC)) { ?>

            <li id="tuto_<?=$row['id'];?>">

                <span><?=$row['id'];?></span>
                <span><?=$row['title'];?></span>

                <?php if($row['published']): ?>

                    <a class="btn published" href="update.php?id=<?=$row['id'];?>&published=<?=$row['published'];?>"><i class="fas fa-check"></i></a>

                <?php else : ?>

                    <a class="btn unpublished" href="update.php?id=<?=$row['id'];?>&published=<?=$row['published'];?>"><i class="far fa-times-circle"></i></a>

                <?php endif; ?>

            </li>

        <?php }

        ?>

    </ul>

</section>

<script src="app.js"></script>

</body>
</html>